import './App.css';
import Nvarbar from './components/Nvarbar';
import TextForm from './components/TextForm';
import About from './components/About';
import React, { useState } from 'react';
import Alert from './components/Alert';
import {
    BrowserRouter as Router,
    Routes,
    Route,
    Link
} from "react-router-dom";


function App() {
    const [mode, setMode] = useState('light');
    const [alert, setAlert] = useState(null);

    const showAlert = (message, type) => {
        setAlert({
            msg: message,
            type: type
        });

        setTimeout(() => {
            setAlert(null)
        }, 1500);
    }

    const toggleMode = () => {
        if (mode === 'light') {
            setMode('dark');
            document.body.style.backgroundColor = '#01303e';
            showAlert("Dark mode has been enabled", "success");
            document.title = "TextUtils Dark mode";
            setInterval(() => {
                document.title = "TextUtils Dark mode install";

            }, 2000);
        } else {
            setMode('light');
            document.body.style.backgroundColor = 'white';
            showAlert("Light mode has been enabled", "success");
            document.title = "TextUtils Light mode";
        }
    }
    return (
        <>
            <Router>
                {/* <Nvarbar title="TextUtils"  about ="About"/> */}
                <Nvarbar mode={mode} toggleMode={toggleMode} />
                <Alert alert={alert} />
                <div className='container'>

                    {/* <About/> */}
                    <Routes>
                        <Route path="/about" element={<About />}>
                            {/* <About /> */}
                        </Route>
                        <Route path="/" element={<TextForm showAlert={showAlert} headingText="Enter text To Modify" mode={mode} toggleMode={toggleMode} />}>
                            {/* <TextForm showAlert={showAlert} headingText ="Enter text To Modify" mode={mode} toggleMode={toggleMode} /> */}
                        </Route>
                    </Routes>
                </div>

            </Router>

        </>
    );
}

export default App;
