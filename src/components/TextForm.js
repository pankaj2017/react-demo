import React, { useState } from 'react';

export default function TextForm(props) {
    const handleUpClick = () => {
        // console.log("Upper case was clicked : "+ text);
        let newText = text.toUpperCase();
        setText(newText);
        props.showAlert("Converted to uppercase!","success");
    }

    const handleLoClick = () => {
        // console.log("Upper case was clicked : "+ text);
        let newText = text.toLowerCase();
        setText(newText);
        props.showAlert("Converted to lowercase!","success");
    }
    const handleClrClick = () => {
        // console.log("Upper case was clicked : "+ text);
        let newText = '';
        setText(newText);
    }
    const handleCopy = () => {
        var text =document.getElementById('exampleFormControlTextarea1');
        text.select();
        navigator.clipboard.writeText(text.value);
    }

    const handleExtraSpaces = () => {
       let newText = text.split(/[ ]+/);
       setText(newText.join(" "));
    }
    
    const handleOnChange = (event) => {
        // console.log(event.target.value);
        setText(event.target.value); 
    }

    const handleConvertSentence = () => {
        let newText = text.split(' ').map( function(word, index) {
            return word.charAt(0).toUpperCase() + word.slice(1).toLowerCase();
        }).join(' ');
        setText(newText);
    }

    
    // Declare a new state variable, which we'll call "count"  
    const [text, setText] = useState("pankaj");

    return (
        <>
        <div className='container' style={{color : props.mode==='dark' ? 'white' : 'black'}} >
            <h1>{props.headingText}</h1>
           <div className="mb-3">
            <textarea className="form-control" value={text} onChange={handleOnChange} id="exampleFormControlTextarea1" rows="3" style={{backgroundColor : props.mode==='dark' ? 'grey' : 'white',color:props.mode==='dark' ? 'white' : 'black'}}></textarea>
            </div> 
            <button type="button" className="btn btn-primary mx-1" onClick={handleUpClick} >Convert to Upper</button>
            <button type="button" className="btn btn-primary mx-1" onClick={handleLoClick} >Convert to Lower</button>
            <button type="button" className="btn btn-primary mx-1" onClick={handleClrClick} >Clear</button>
            <button type="button" className="btn btn-primary mx-1" onClick={handleCopy} >Copy Text</button>
            <button type="button" className="btn btn-primary mx-1" onClick={handleExtraSpaces} >Remove Extra Spaces</button>
            <button type="button" className="btn btn-primary mx-1" onClick={handleConvertSentence} >Convert to Sentence</button>




        </div>
        <div className='container my-2' style={{color : props.mode==='dark' ? 'white' : 'black'}}>
            <h1>Your Text Summary</h1>
            <p>{text.split(" ").filter((element)=>{return element.length!==0}).length} words and {text.length} characters</p>
            <p>{0.008 * text.split(" ").length } Minutes read</p>
            <h3>Preview</h3>
            <p>{text.length > 0 ? text :'Enter something to preview it here.'}</p>
        </div>
        </>

    )
}
